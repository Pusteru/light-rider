package field;

public class Point extends java.awt.Point {
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void sum(Point position) {
        this.x += position.x;
        this.y += position.y;
    }
}
