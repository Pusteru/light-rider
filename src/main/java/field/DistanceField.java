package field;

import java.util.Arrays;
import java.util.stream.IntStream;

public class DistanceField {
    private int[][] field;


    public DistanceField(Field field) {
        this.field = new int[field.getWidth()][field.getHeight()];
        for (int i = 0; i < field.getWidth(); ++i) {
            for (int j = 0; j < field.getHeight(); ++j) {
                this.field[i][j] = 0;
            }
        }
    }

    public void sum(Point point, Point neighbour) {
        this.field[neighbour.x][neighbour.y] = this.field[point.x][point.y] + 1;
    }

    public int getMaxDistance() {
        IntStream stream = Arrays.stream(this.field).flatMapToInt(Arrays::stream);
        return stream.max().getAsInt();
    }
}
