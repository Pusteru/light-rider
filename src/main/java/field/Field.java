// // Copyright 2016 riddles.io (developers@riddles.io)

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//  
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.

package field;


import move.MoveType;

import java.util.ArrayList;
import java.util.List;

/**
 * field.Field
 *
 * Handles everything that has to do with the field, such 
 * as storing the current state and performing calculations
 * on the field.
 *
 * @author Jim van Eeden <jim@riddles.io>
 */

public class Field {

    private int myId;
    private int width;
    private int height;
    private String[][] field;
    private boolean[][] visited;
    private Point myPosition;
    private Point enemyPosition;

    public Field(int myId, int width, int height, String[][] field, boolean[][] visited, Point myPosition, Point enemyPosition) {
        this.myId = myId;
        this.width = width;
        this.height = height;
        this.field = field;
        this.visited = visited;
        this.myPosition = myPosition;
        this.enemyPosition = enemyPosition;
    }

    public Field() {

    }

    /**
     * Initializes and clears field
     * @throws Exception exception
     */
    public void initField() throws Exception {
        try {
            this.field = new String[this.width][this.height];
            this.visited = new boolean[this.width][this.height];
        } catch (Exception e) {
            throw new Exception("Error: trying to initialize field while field "
                    + "settings have not been parsed yet.");
        }

        clearField();
    }

    /**
     * Clears the field
     */
    private void clearField() {
        for (int y = 0; y < this.height; y++) {
            for (int x = 0; x < this.width; x++) {
                this.field[x][y] = ".";
                this.visited[x][y] = false;
            }
        }

        this.myPosition = null;
        this.enemyPosition = null;
    }

    /**
     * Parse field from comma separated String
     * @param s input from engine
     */
    public void parseFromString(String s) {
        clearField();
        
        String[] split = s.split(",");
        int x = 0;
        int y = 0;

        for (String value : split) {
            this.field[x][y] = value;
            this.visited[x][y] = !value.equals(".") && !value.equals(String.valueOf(this.myId));

            if (this.field[x][y].equals(this.myId + "")) {
                this.myPosition = new Point(x, y);
            }

            if (!this.field[x][y].equals(".") && !this.field[x][y].equals("X") && !this.field[x][y].equals(this.myId + "")) {
                this.enemyPosition = new Point(x, y);
            }
                            
            if (++x == this.width) {
                x = 0;
                y++;
            }
        }
    }

    public boolean exists(int x, int y) {
        return x >= 0 && y >= 0 && x < this.width && y < this.height && this.field[x][y].equals(".");
    }

    public void setMyId(int id) {
        this.myId = id;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Point getMyPosition() {
        return this.myPosition;
    }

    public boolean exists(Point direction) {
        return exists(direction.x, direction.y);
    }

    public boolean isVisited(Point point) {
        return this.visited[point.x][point.y];
    }

    public void visited(Point point) {
        this.visited[point.x][point.y] = true;
    }

    public List<Point> getNeighbours(Point point) {
        List<Point> neighbours = new ArrayList<>();
        neighbours.addAll(getNeighbours(point, MoveType.RIGHT));
        neighbours.addAll(getNeighbours(point, MoveType.LEFT));
        neighbours.addAll(getNeighbours(point, MoveType.UP));
        neighbours.addAll(getNeighbours(point, MoveType.DOWN));
        return neighbours;
    }

    private List<Point> getNeighbours(Point point, MoveType moveType) {
        List<Point> neighbours = new ArrayList<>();
        Point newPoint = new Point(point.x, point.y);
        newPoint.sum(moveType.getSumPosition());
        if (exists(newPoint) && !this.visited[newPoint.x][newPoint.y]) {
            neighbours.add(newPoint);
        }
        return neighbours;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public Point getEnemyPosition() {
        return this.enemyPosition;
    }

    public Field moveEnemy(MoveType possibleMove) {
        Field newField = new Field(this.myId, this.width, this.height, this.field, this.visited, this.myPosition, this.enemyPosition);
        newField.field[this.enemyPosition.x][this.enemyPosition.y] = "X";
        Point point = possibleMove.getSumPosition();
        point.sum(this.enemyPosition);
        String enemyId = this.myId == 0 ? "1" : "0";
        newField.field[point.x][point.y] = enemyId;
        this.visited[point.x][point.y] = true;


        return newField;
    }
}