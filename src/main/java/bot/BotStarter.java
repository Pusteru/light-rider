// // Copyright 2016 riddles.io (developers@riddles.io)

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.

package bot;

import field.DistanceField;
import field.Field;
import move.Move;
import move.MoveType;

import field.Point;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;


public class BotStarter {

    public Move doMove(BotState state) {
        MoveType moveType = null;
        MoveType lastMoveType = state.getLastDirection();

        Field field = state.getField();
		Point position = field.getMyPosition();

        moveType = getMoveType(lastMoveType, field);

		if (moveType != MoveType.PASS) {
            state.setLastDirection(moveType);
        }

		return new Move(moveType);
    }

    public MoveType getMoveType(MoveType lastMoveType, Field field) {
        MoveType moveType;
        moveType = firstMove(lastMoveType);

        if (lastMoveType != null) {
            moveType = getMoveDFS(lastMoveType, field);
            if (moveType == null) {
                moveType = lastMoveType;
            }
        }
        return moveType;
    }

    private MoveType getMoveDFS(MoveType lastMoveType, Field field) {
        MoveType moveType = null;
        int max = -1000;
        for (Field movedField : getFieldsWithEnemyMoved(field)) {
            for (MoveType possibleMove : MoveType.values()) {
                if (possibleMove != lastMoveType.getOpposite() && canMove(movedField, possibleMove, movedField.getMyPosition())) {
                    int distance = getMaxDistance(movedField, possibleMove, movedField.getMyPosition());
                    int enemyDistance = dfs(movedField, movedField.getEnemyPosition()).getMaxDistance();
                    if (distance - enemyDistance > max) {
                        max = distance - enemyDistance;
                        moveType = possibleMove;
                    }
                }
            }
        }
        return moveType;
    }

    private List<Field> getFieldsWithEnemyMoved(Field field) {
        List<Field> fields = new ArrayList<>();
        for (MoveType possibleMove : MoveType.values()) {
            if (canMove(field, possibleMove, field.getEnemyPosition())) {
                fields.add(field.moveEnemy(possibleMove));
            }
        }
        return fields;
    }

    private boolean canMove(Field field, MoveType moveType, Point position) {
        Point point = moveType.getSumPosition();
        point.sum(position);
        return field.exists(point);
    }

    private MoveType firstMove(MoveType lastMoveType) {
        if (lastMoveType == null) {
            return MoveType.getRandomExcluding(MoveType.PASS);
        }
        return null;
    }

    public int getMaxDistance(Field field, MoveType move, Point position) {
        Point point = move.getSumPosition();
        point.sum(position);
        return dfs(field, point).getMaxDistance();
    }

    public DistanceField dfs(Field field, Point actualPosition) {
        DistanceField distanceField = new DistanceField(field);
        Stack<Point> stack = new Stack<>();
        stack.push(actualPosition);
        while (!stack.empty()) {
            Point point = stack.pop();
            if (!field.isVisited(point)) {
                field.visited(point);
                for (Point neighbour : field.getNeighbours(point)) {
                    distanceField.sum(point, neighbour);
                    stack.push(neighbour);
                }
            }
        }
        return distanceField;
    }

    private MoveType getRandomMoveType(MoveType lastMoveType, Field field, Point position) {
        MoveType moveType1 = MoveType.getRandomExcluding(lastMoveType.getOpposite());
        Point directionX = moveType1.getSumPosition();
        directionX.translate(position.x, position.y);
        if (field.exists(directionX)) {
            return moveType1;
        }
        return null;
    }

	/**
     * Main method for the bot. Creates a parser and runs it.
     */
 	public static void main(String[] args) {
 		BotParser parser = new BotParser(new BotStarter());
 		parser.run();
 	}
 }
