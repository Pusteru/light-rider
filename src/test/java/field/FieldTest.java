package field;

import bot.BotStarter;
import move.MoveType;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class FieldTest {
    @Test
    public void parseFromString() throws Exception {
        String inputField = ".,0,.,.,.,.,.,.,.,.,.,.,.,.,1,.,.,x,.,.,.,.,.,.,.,.,.,.,.,.,x,.,.,x,.,.,.,.,.,.,.,.,.,.,.,.,x,.,.,x,x,.,.,.,.,.,.,.,.,.,.,x,x,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.";
        Field field = new Field();
        field.setHeight(16);
        field.setWidth(16);
        field.setMyId(0);
        field.initField();
        field.parseFromString(inputField);


        BotStarter botStarter = new BotStarter();
        MoveType moveType = botStarter.getMoveType(MoveType.UP, field);

        assertEquals(moveType, MoveType.LEFT);
    }

    @Test
    public void try_dfs() throws Exception {
        String inputField = ".,.,.,.,0,.,.,.,.";
        Field field = new Field();
        field.setHeight(3);
        field.setWidth(3);
        field.setMyId(0);
        field.initField();
        field.parseFromString(inputField);


        BotStarter botStarter = new BotStarter();
        DistanceField distanceField = botStarter.dfs(field, field.getMyPosition());
        int distance = distanceField.getMaxDistance();

        assertThat(distance, is(8));


    }
}